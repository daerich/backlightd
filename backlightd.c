#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>

#include "common.h"

static void handler(int);
static int read_drv(char *,int,int);
static void set_brightness(float);
static void loop(int);

enum COMM{READ,WRITE,INTEL,AMD};

static int brightness = 0;
static int max_bright = 0;
static volatile sig_atomic_t eflag = 0;
static volatile sig_atomic_t sflag = 0;
static enum COMM mode = 0;

#define VIDEOGROUP VIDEOGROUPT

#ifndef DEBUG

#define INTEL_STRING "/sys/class/backlight/intel_backlight/"
#define ACPI_STRING  "/sys/class/backlight/acpi_video0/"
#define CONFIG_STRING CONFIGS

#else

#define INTEL_STRING DPATH
#define ACPI_STRING DPATH
#define CONFIG_STRING DPATH "backlight_d.conf"

#endif

int main(int argc, char**argv)
{
	openlog("backlightd",LOG_PID,LOG_DAEMON);
	struct sigaction sigconf = { 
		handler,
		0,
		0,
		SA_RESTART,
		0
	};
	sigaction(SIGTERM, &sigconf, NULL);
	sigaction(SIGUSR1, &sigconf, NULL);
	
	syslog(LOG_NOTICE,"Started backlightd! Version "VERSION);

	if(access(INTEL_STRING "brightness",(R_OK/*|W_OK*/)) == 0){
		mode=INTEL;
		syslog(LOG_NOTICE,"Using intel driver!");
	}
	else
		if(access(ACPI_STRING "brightness",(R_OK/*|W_OK*/)) == 0){
			mode=AMD;
			syslog(LOG_WARNING,"Using acpi driver!");
		}
		else{
			if(errno == EACCES)
				syslog(LOG_WARNING,"Can't write to file!;Exiting!");
			else
				syslog(LOG_WARNING,"Could not find proper drivers!;Exiting!");
			closelog();
			return 1;
		}
	loop(1);

	while(!eflag){
		if(sflag){
			loop(1);
			syslog(LOG_NOTICE,"Reloading backlight config");
			sflag = 0;
		}
		pause();
	}
	loop(0);
	
	syslog(LOG_WARNING,"Terminating on SIGTERM");

	closelog();
	return 0;
}


static void handler(int signal) /* Don't use non-async logic */
{
	if(signal == SIGTERM)
		eflag = 1;
	else if(signal == SIGUSR1)
		sflag = 1;

}

static int read_drv(char * filen,int mode, int nbrights)
{
	FILE* backlight = NULL;	
	if(mode == READ){ 
		backlight=fopen(filen,"r");
		
		char ch = 0;
		char * buf = NULL;
		int length = 0;
		int res = 0;
		for(;(ch=fgetc(backlight))!= EOF;length++){
			if(ch == '\n' ) /* We don't need newlines */
				break;

			buf=addread(buf,ch,length);
		}
		buf=addread(buf,'\0',length);
		
		res = atoi(buf);
		free(buf);	
		fclose(backlight);
		return res;
	}
	else if(mode == WRITE){
		backlight=fopen(filen,"r+");
		fprintf(backlight,"%d",nbrights);
		fclose(backlight);
		return 0;
	}
	
	return 0; /* Please gcc */	


}

static void loop(int firstrun)
{
	if( mode == INTEL){
		max_bright=read_drv(INTEL_STRING "max_brightness",READ,0);
			if(firstrun){
				if(access(CONFIG_STRING,(R_OK|W_OK)) == 0)
				{
					brightness=read_drv(CONFIG_STRING,READ,0);
					set_brightness(brightness);
				}
				else{
					syslog(LOG_WARNING,"Can't find config_file!");
					closelog();
					exit(1);
				}
				firstrun = 0;
			}
			else{
				brightness=read_drv(INTEL_STRING "brightness", READ,0);
				brightness=(((float)brightness/max_bright) * 100);
				truncate(CONFIG_STRING ,0);
				read_drv(CONFIG_STRING ,WRITE,brightness);
			}
	}
	else
		if(mode == AMD) {
			max_bright=read_drv(ACPI_STRING "max_brightness",READ,0);

				if(firstrun){
					if(access(CONFIG_STRING,(R_OK|W_OK)) == 0)
					{
						brightness=read_drv(CONFIG_STRING,READ,0);
						set_brightness(brightness);
					}
					else{
						syslog(LOG_WARNING,"Can't find config_file!");
						closelog();
						exit(1);
					}
				}
				else{
					brightness=read_drv(ACPI_STRING "brightness", READ,0);
					brightness=(((float)brightness/max_bright) * 100);
					truncate(CONFIG_STRING,0);
					read_drv(CONFIG_STRING,WRITE,brightness);
				}
		}
		
	
#ifdef DEBUG
	printf("Bright:%d,Max_Bright:%d\n",brightness,max_bright);
	fflush(stdout); /* Flush before sleep */
#endif


}



static void set_brightness(float value)
{	
	if((value < 10.0) || (value > 100.0) ){
		syslog(LOG_WARNING,"Error wrong value of %f!",value);
		return;
	}
	
	float scale = (value/100) * (float)max_bright;
	if(mode == INTEL){
		read_drv(INTEL_STRING "brightness",WRITE,scale);
	}
	else if(mode == AMD){
		read_drv(ACPI_STRING "brightness",WRITE,scale);
	}
	brightness=(int)value;
}	


